import React from 'react';
import './App.css';
import {DashboardHome} from "./features/dashboard";

function App() {
     return (
        <>
            <DashboardHome/>
        </>
    );
}

export default App;
