import HttpClient from "./HttpClient";
import APIs from "../constants/APIs";

const ApiDashboard = {

    loginWithPassword: async (data: any) => {
        const response = await HttpClient.post(`${APIs.POST_GRAPHQL}`, data);
        return response?.data;
    },
}

export default ApiDashboard;
