import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';
import {ROOT_API} from "../constants/commons";

const HttpClient = axios.create({
    baseURL: ROOT_API,
    headers: {
        'Content-Type': 'application/json',
    },
    timeout: 30000,
});

// Add a request interceptor
HttpClient.interceptors.request.use(
    function (config: AxiosRequestConfig) {
        return config;
    },
    function (error) {
        // Do something with request error
        return Promise.reject(error);
    },
);

// Add a response interceptor
HttpClient.interceptors.response.use(
    function (response: AxiosResponse<any>) {
        return response.data;
    },
    function (error) {
        return Promise.reject(error.response.data);
    },
);

export default HttpClient;
