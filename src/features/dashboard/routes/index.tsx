import React, {useEffect, useState} from 'react';
import ApiDashboard from "../../../api/ApiDashboard";

const DashboardHome = () => {
    const [isdata, setData] = useState([])

    const getData = async () => {
        const data = {
            query: "{products(filter:{}){items {name,sku}}}",
            variables: {}
        }
        const res = await ApiDashboard.loginWithPassword(data);
        console.log("-------",res?.products)
        setData(res?.products)
    }
    useEffect(() => {
        getData()
    }, []);

    return (
        <div>
            <div>
                {isdata.length > 0 && isdata.map((item: any) =>
                    <div>
                        {item}
                    </div>
                )}
            </div>
            <div>1231231</div>
        </div>
    );
};

export default DashboardHome;
